import Mock from 'mockjs'
import { param2Obj, getQueryObject } from '../src/utils'

// import user from './api/user'
import dynamic from './api/dynamic'

const mocks = [
  // ...user,
  ...dynamic
]

export function mockXHR() {
  window.can_use_qs = false
  Mock.XHR.prototype.proxy_send = Mock.XHR.prototype.send
  Mock.XHR.prototype.send = function() {
    if (this.custom.xhr) {
      this.custom.xhr.withCredentials = this.withCredentials || false

      if (this.responseType) {
        this.custom.xhr.responseType = this.responseType
      }
    }
    this.proxy_send(...arguments)
  }

  function XHR2ExpressReqWrap(respond) {
    return options => {
      let result = null
      if (respond instanceof Function) {
        const { body, type, url } = options
        const queryBody = getQueryObject(body)
        result = respond({
          method: type,
          body: queryBody,
          query: param2Obj(url)
        })
      } else {
        result = respond
      }
      return Mock.mock(result)
    }
  }

  for (const i of mocks) {
    Mock.mock(new RegExp(i.url), i.type || 'get', XHR2ExpressReqWrap(i.response))
  }
}

const responseFake = (url, type, respond) => {
  return {
    url: new RegExp(`/mock${url}`),
    type: type || 'get',
    response(req, res) {
      console.log(res)
      res.json(Mock.mock(respond instanceof Function ? respond(req, res) : respond))
    }
  }
}

export default mocks.map(route => {
  return responseFake(route.url, route.type, route.response)
})

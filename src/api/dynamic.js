// 动态页
import requst from '@/utils/requst'
// 创建动态
export function createDynamic(data) {
  const {
    content,
    photo
  } = data
  return requst({
    url: 'dynamic/user/create',
    method: 'post',
    data: {
      content,
      photo
    }
  })
}
// 获取动态
export function getDynamicList(data = {}) {
  const {
    id = 0,
    page = 1,
    size = 10000
  } = data
  const url = `dynamic/user/get/${id}/${page}/${size}`
  return requst({
    url: url, // `dynamic/user/get`,
    method: 'get'
  })
}
// 查看一个动态 是否点赞
export function getStar(data = {}) {
  const {
    id: dynamic_id
  } = data
  return requst({
    url: '/dynamic/user/is-like',
    method: 'post',
    data: {
      dynamic_id
    }
  })
}

// 点赞操作
export function createStar(data = {}) {
  const {
    id: dynamic_id
  } = data
  return requst({
    url: '/dynamic/user/like',
    method: 'post',
    data: {
      dynamic_id
    }
  })
}

// 取消点赞
export function delStar(data = {}) {
  const {
    id: dynamic_id
  } = data
  return requst({
    url: '/dynamic/user/unlike',
    method: 'delete',
    data: {
      dynamic_id
    }
  })
}
// 删除动态
export function delDynamic(data) {
  const {
    id: dynamic_id
  } = data
  return requst({
    url: '/dynamic/user/delete/' + dynamic_id,
    method: 'delete'
  })
}
// 评论动态
export function createComment(data) {
  const {
    dynamic_id,
    content
  } = data
  console.log(data)
  return requst({
    url: '/dynamic/user/comment',
    method: 'post',
    data: {
      dynamic_id,
      content
    }
  })
}
// 获取某个动态的所有评论 获取某个动态的所有评论
export function getComment(data) {
  const {
    id = 1
  } = data
  return requst({
    url: '/dynamic/user/get-comment/' + id,
    method: 'get'
  })
}
// 删除评论
export function dynamic5(data) {
  const {
    dynamic_id,
    comment_id
  } = data
  return requst({
    url: '/dynamic/user/uncomment',
    method: 'delete',
    data: {
      dynamic_id,
      comment_id
    }
  })
}
// 添加回复
export function th_createComment(data) {
  const {
    pid,
    dynamic_id,
    comment_id,
    to_id,
    content
  } = data
  return requst({
    url: '/dynamic/user/reply',
    method: 'post',
    data: {
      pid,
      dynamic_id,
      comment_id,
      to_id,
      content
    }
  })
}
// 删除回复
export function dynamic3(data) {
  const {
    dynamic_id,
    comment_id,
    reply_id
  } = data
  return requst({
    url: '/dynamic/user/unreply',
    method: 'delete',
    data: {
      dynamic_id,
      comment_id,
      reply_id
    }
  })
}
// 获取评论的回复
export function dynamic2(data) {
  const {
    dynamic_id,
    comment_id
  } = data
  return requst({
    // url:'/dynamic/user/get-reply/'+dynamic_id+'/'+comment_id,
    url: `/dynamic/user/get-reply/${dynamic_id}/${comment_id}`,
    method: 'get'
  })
}
// 智能匹配
export function match(data = {}) {
  // const {
  //   id = 5
  // } = data
  return requst({
    url: '/user/object/get',
    method: 'get'
  })
}
// 增加文章 浏览量
export function dynamic1(data = {}) {
  const {
    id = 5
  } = data
  return requst({
    url: `/details/activity/view/${id}`,
    method: 'get'
  })
}

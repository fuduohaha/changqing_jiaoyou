// 获取资料
import requst from '@/utils/requst'
// 兴趣爱好
export function aihao(data) {
  return requst({
    url: '/details/hobby/get',
    methods: 'get'
  })
}
// 系统部门
export function bumen() {
  return requst({
    url: '/details/department/get',
    methods: 'get'
  })
}
// 获取星座
export function getConstellation(data) {
  return requst({
    url: '/details/constellation/get',
    methods: 'get'
  })
}
// 获取banner图
export function banner(data) {
  return requst({
    url: '/details/banner/get',
    methods: 'get'
  })
}
// 获取活动
export function active(data = {}) {
  const {
    id = 0
  } = data
  return requst({
    url: '/details/activity/get/' + id,
    methods: 'get'
  })
}

import requst from '@/utils/requst'
// 获取个人信息
export function getUserInfo(data) {
  const {
    id = 4
  } = data
  return requst({
    url: '/details/user/get/' + id,
    methods: 'get'
  })
}
// 完善个人信息一
export function infoComplete(data) {
  const {
    avatar,
    gender,
    birthday,
    constellation,
    wechat
  } = data
  return requst({
    url: '/user/profile/first',
    methods: 'put',
    data: {
      avatar,
      gender,
      birthday,
      constellation,
      wechat
    }
  })
}
// 完善个人信息二
export function info(data) {
  const {
    bio,
    hobby,
    album
  } = data
  return requst({
    url: '/user/profile/second',
    methods: 'put',
    data: {
      bio,
      hobby,
      album
    }
  })
}
// 修改个人资料
export function revise(data) {
  const {
    avatar,
    birthday,
    constellation,
    wechat,
    bio,
    hobby
  } = data
  return requst({
    url: '/user/profile/change',
    methods: 'put',
    data: {
      avatar,
      birthday,
      constellation,
      wechat,
      bio,
      hobby
    }
  })
}
// 上传头像
export function uplod(data) {
  return requst({
    url: '/attachment/upload',
    methods: 'post'
  })
}

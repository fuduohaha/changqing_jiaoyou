import requst from '@/utils/requst'
// 用户登陆
export function login(data) {
  const {
    account,
    password
  } = data
  return requst({
    url: '/user/login',
    method: 'post',
    data: {
      account,
      password
    }
  })
}

// 修改密码
export function resetPwd(data) {
  const {
    oldpassword,
    newpassword,
    newpassword_confirm
  } = data
  return requst({
    url: '/user/changepwd',
    method: 'put',
    data: {
      oldpassword,
      newpassword,
      newpassword_confirm
    }
  })
}

// 退出账号
export function exit(data) {
  return requst({
    url: '/user/logout',
    method: 'post'
  })
}


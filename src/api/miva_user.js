import request from '@/utils/request';

// 用户登录
export function login(data) {
    const {
        account,
        password
    } = data;
    return request({
        url: '/user/login',
        method: 'post',
        data: {
            account,
            password
        }
    });
}
// 退出登录
export function logout() {
    return request({
        url: '/user/logout',
        method: 'post'
    });
}
// 获取用户信息
export function getUserInfo(data = {}) {
    const { id = false } = data;
    const url = id ? `/details/user/get/${id}` : '/details/user/get';
    return request({
        url,
        method: 'get'
    });
}
// 修改密码
export function changepwd(data) {
    const {
        oldpassword,
        newpassword,
        newpassword_confirm
    } = data;
    return request({
        url: '/user/changepwd',
        method: 'put',
        data: {
            oldpassword,
            newpassword,
            newpassword_confirm
        }
    });
}
// 完善个人信息第一步
export function profileFirst(data) {
    const {
        avatar = '123456', // 头像地址
        gender = 1, // 1男2女3未知
        birthday = '2019-08-20', // 畜生年月
        constellation = 1, // 星座ID
        wechat = '123452226'// 微信号
    } = data;
    return request({
        url: `/user/profile/first`,
        method: 'put',
        data: {
            avatar,
            gender,
            birthday,
            constellation,
            wechat
        }
    });
}
// 完善个人信息第二步
export function profileSecond(data) {
    const {
        bio = '', // 格言
        hobby = '', // 业余爱好ID
        album = []
    } = data;
    return request({
        url: `/user/profile/second`,
        method: 'put',
        data: {
            bio,
            hobby,
            album
        }
    });
}
// 修改个人资料
export function profileChange(data) {
/* eslint-disable */
  let {
    avatar = '/uploads/20190820/ab9c1daa53a0729bfe103a5b27679252.jpg', // 头像
    birthday = '2019-08-20', // 出生年月
    constellation = 1, // 星座ID
    wechat = '', // 微信账号
    bio = '', // 交友格言
    hobby = '1,2',// 业余爱好ID
    gender
  } = data
  /* eslint-enable */
    hobby = hobby.toString();
    // const firstIndex = avatar.indexOf('.com/')
    // const lastIndex = avatar.indexOf('?')
    // const image = avatar.slice(firstIndex + 5, lastIndex)
    return request({
        url: `/user/profile/change`,
        method: 'put',
        data: {
            avatar,
            birthday,
            constellation,
            wechat,
            bio,
            hobby
            // gender
        }
    });
}
// 获取个人相册
export function getAlbum(data) {
    const {
        id
    } = data;
    return request({
        url: `/details/album/get/${id}`,
        method: 'get'
    });
}


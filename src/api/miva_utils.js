import request from '@/utils/request'

// 上传照片 头像
export function uploadImage(formData) {
  console.log(formData)
  return request({
    url: `/attachment/upload`,
    method: 'post',
    data: formData,
    headers: {
      'Content-Type': 'multipart/from-data'
    }
  })
}


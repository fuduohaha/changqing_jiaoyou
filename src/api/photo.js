import requst from '@/utils/requst'
// 修改相册
export function setPhoto(data) {
  const {
    album
  } = data
  console.log(album)
  return requst({
    url: '/user/album/change',
    method: 'put',
    data: {
      album
    }
  })
}
// 获取个人相册
export function getPhoto(data) {
  const {
    id = 4
  } = data
  return requst({
    url: '/details/album/get/' + id,
    method: 'get'
  })
}

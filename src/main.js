import Vue from 'vue'
import App from './App.vue'
import store from './store/'
import router from './router'

// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// Vue.use(ElementUI)

import Vant from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant)

import './permission' // permission control

import { mockXHR } from '../mock'
if (process.env.NODE_ENV === 'production') {
  mockXHR()
} else {
  // 手动控制开启
  mockXHR()
}

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/Releasedynamic',
            name: 'Releasedynamic',
            component: () => import(/* webpackChunkName: "Releasedynamic" */ './views/Releasedynamic/index.vue'),
            meta: { title: '发布动态', canBack: true }
        }, {
            path: '/profile',
            name: 'profile',
            component: () => import(/* webpackChunkName: "profile" */ './views/profile/index.vue'),
            meta: { title: '我的资料', canBack: true }
        }, {
            path: '/PersonalityInfo',
            name: 'PersonalityInfo',
            component: () => import(/* webpackChunkName: "PersonalityInfo" */ './views/PersonalityInfo/index.vue'),
            meta: { title: '个性信息' }
        }, {
            path: '/Othersphoto',
            name: 'Othersphoto',
            component: () => import(/* webpackChunkName: "Othersphoto" */ './views/Othersphoto/index.vue'),
            meta: { title: '他人相册' }
        }, {
            path: '/Othersdynami',
            name: 'Othersdynami',
            component: () => import(/* webpackChunkName: "Othersdynami" */ './views/Othersdynami/index.vue'),
            meta: { title: '他人动态' }
        }, {
            path: '/myphoto',
            name: 'myphoto',
            component: () => import(/* webpackChunkName: "myphoto" */ './views/myphoto/index.vue'),
            meta: { title: '我的相册' }
        }, {
            path: '/Mydynamic',
            name: 'mydynamic',
            component: () => import(/* webpackChunkName: "Mydynamic" */ './views/Mydynamic/index.vue'),
            meta: { title: '我的动态' }
        }, {
            path: '/my',
            name: 'my',
            component: () => import(/* webpackChunkName: "my" */ './views/my/index.vue'),
            meta: {  navbar: true, canBack: false }
        }, {
            path: '/MatchInfo',
            name: 'MatchInfo',
            component: () => import(/* webpackChunkName: "MatchInfo" */ './views/MatchInfo/index.vue'),
            meta: { title: '匹配信息' }
        }, {
            path: '/login',
            name: 'login',
            component: () => import(/* webpackChunkName: "login" */ './views/login/index.vue')
        }, {
            path: '/home',
            name: 'home',
            component: () => import(/* webpackChunkName: "home" */ './views/home/index.vue'),
            meta: {  navbar: true, canBack: false }
        }, {
            path: '/dynamic',
            name: 'dynamic',
            component: () => import(/* webpackChunkName: "dynamic" */ './views/dynamic/index.vue'),
            meta: {  navbar: true, canBack: false }
        }, {
            path: '/BasicInfo',
            name: 'BasicInfo',
            component: () => import(/* webpackChunkName: "BasicInfo" */ './views/BasicInfo/index.vue'),
            meta: { title: '基本信息' }
        }, {
            path: '/Activities',
            name: 'Activities',
            component: () => import(/* webpackChunkName: "Activities" */ './views/Activities/index.vue'),
            meta: { title: '活动页面' }
        }, {
            path: '/ChangePasw',
            name: 'ChangePasw',
            component: () => import(/* webpackChunkName: "ChangePasw" */ './views/ChangePasw/index.vue'),
            meta: { title: '修改密码页面' }
        }, {
            path: '/ActiveDetails',
            name: 'ActiveDetails',
            component: () => import(/* webpackChunkName: "ActiveDetails" */ './views/ActiveDetails/index.vue'),
            meta: { title: '活动详情页面' }
        },{
            path: '/filtrate',
            name: 'filtrate',
            component: () => import(/* webpackChunkName: "filtrate" */ './views/filtrate/index.vue'),
            meta: { title: '条件筛选' }
        },{
            path: '/filtrateAll',
            name: 'filtrateAll',
            component: () => import(/* webpackChunkName: "filtrateAll" */ './views/filtrateAll/index.vue'),
            meta: { title: '所有人' }
        }
    ]
});

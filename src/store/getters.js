const getters = {
  header: state => state.app.header,
  navbar: state => state.app.navbar,
  maskShow: state => state.app.maskShow,
  userInfo: state => state.user.userInfo,
  token: state => state.user.token,
  xingzuo: state => state.utils.xingzuo,
  bumen: state => state.utils.bumen
}

export default getters

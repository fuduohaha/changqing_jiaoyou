const state = {
  header: {
    opened: true,
    canBack: false,
    canClose: false,
    title: ''
  },
  maskShow: false,
  navbar: {
    opened: false
  }
}

const mutations = {
  TOGGLE_HEADER: (state, meta) => {
    state.header.opened = !!meta.title
    state.header.title = meta.title
    state.header.canBack = meta.canBack !== false
  },
  TOGGLE_NAVBAR: (state, meta) => {
    state.navbar.opened = meta.navbar || false
  },
  TOGGLE_MASK: (state, meta) => {
    state.maskShow = meta || false
  }
}

const actions = {
  toggleHeader({ commit }, meta) {
    commit('TOGGLE_HEADER', meta)
  },
  toggleNavbar({ commit }, meta) {
    commit('TOGGLE_NAVBAR', meta)
  },
  toggleMask({ commit }, meta) {
    commit('TOGGLE_MASK', meta)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

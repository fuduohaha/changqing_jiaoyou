const state = {
  xingzuo: [],
  bumen: []
}

const mutations = {
  SAVE_XINGZUO: (state, data) => {
    state.xingzuo = data || []
  },
  SAVE_BUMEN: (state, data) => {
    state.bumen = data || []
  }
}

const actions = {
  saveXinzuo({ commit }, data) {
    commit('SAVE_XINGZUO', data)
  },
  saveBumen({ commit }, data) {
    commit('SAVE_BUMEN', data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

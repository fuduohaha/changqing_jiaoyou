import axios from 'axios'
// import qs from 'qs'
import sotre from '@/store'
import { getToken } from '@/utils/auth'

const service = axios.create({
  baseURL: 'http://api.unmcc.com',
  timeout: 15000,
  validateStatus: status => {
    return status < 600
  },
  headers: {
    // 'Content-Type': 'application/json;charset=utf-8'
    // 'Content-Type': 'application/ x - www - form - urlencoded;charset=utf-8'
  }
})

// 请求拦截器
service.interceptors.request.use(
  config => {
    // config.data = qs.stringify(config.data)

    const token = `Bearer ${getToken()}`

    if (sotre.getters.token) {
      config.headers['authorization'] = token
    }

    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  response => {
    const res = response.data
    // console.log('reqest.js log:', res)
    return res
  },
  error => {
    return Promise.reject(error)
  }
)

export default service

const port = process.env.port || process.env.npm_config_port || 2333;
module.exports = {
    publicPath: '/', // process.env.NODE_ENV === 'production' ? '../changqing/' : '/',
    devServer: {
        port,
        proxy: {
            [process.env.VUE_APP_BASE_API]: {
                target: `http://127.0.0.1:${port}/mock`,
                changeOrigin: true,
                pathRewrite: {
                    ['^' + process.env.VUE_APP_BASE_API]: ''
                }
            }
        },
        after: require('./mock/mock-server.js')
    }
};
